Creado por ignacio.garcia@uca.es
fecha: 2015-03-09

Librería PySide http://zetcode.com/gui/pysidetutorial/

PySide es biblioteca de Python para crear interfaces de usuario gráficas multiplataforma. 
Es un enlace de Python para el marco de Qt. 
La librería QT es una de las más potentes bibliotecas GUI (interfaces gráficas de usuario). 
Es desarrollado por Digia y proyecto Qt.

pass



La instalación de PySide la haremos descargando desde 
<a href='http://www.lfd.uci.edu/~gohlke/pythonlibs/'>aquí</a>
el fichero whl y ejecutando la orden <code>pip PySide-1.2.2XXXXXXX.whl install</code>

pass



Un primer ejemplo sencillo que muestra una ventana es el siguiente 

import sys
from PySide import QtGui
# Ya tenemos una ventana principal, la ventana de pyFMSD
try:
	app = QtGui.QApplication(['hola'])
except:
	pass
wid = QtGui.QWidget()
wid.resize(250, 150)
wid.setWindowTitle('Simple')
wid.show()
try:
	sys.exit(app.exec_())
except:
	pass



La forma más usual de usar la objetos de PySide es la siguiente.

import sys
from PySide import QtGui
class Example(QtGui.QWidget):
    def __init__(self):
        super(Example, self).__init__()
        self.initUI()
    def initUI(self):
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Icon')
        self.setWindowIcon(QtGui.QIcon('icons/ic_menu_compass.png'))        
        self.show()
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())



Ahora, en lugar de llamar a la función <code>main</code> ejecutamos el código siguiente 
que nos muestra una ventana. 
De nuevo no ejecutamos <code>app = QtGui.QApplication(sys.argv)</code> ya que 
nuestra aplicación <code>pyFMSD</code> es una  <code>QApplication</code> y solamente 
podemos tener una sola de estas clases en una misma aplicación.

ex = Example()





Ponemos un botón en nuestra ventana


class Example1(QtGui.QWidget):
    def __init__(self):
		super(Example1, self).__init__()
		QtGui.QToolTip.setFont(QtGui.QFont('SansSerif', 10))
		self.setToolTip('This is a QWidget widget')
		btn = QtGui.QPushButton('Button', self)
		btn.setToolTip('This is a QPushButton widget')		
		btn.resize(btn.sizeHint())
		btn.move(50, 50)
		self.setGeometry(300, 300, 250, 150)
		self.setWindowTitle('Tooltips')    
		self.show()





Llamamos de nuevo a <code>Example</code>

ex = Example1()



Conectando un botón que cierre la ventana

from PySide import QtGui, QtCore
class Example2(QtGui.QWidget):
    def __init__(self):
        super(Example2, self).__init__()
        qbtn = QtGui.QPushButton('Quit', self)
        qbtn.clicked.connect(self.close)
        qbtn.resize(qbtn.sizeHint())
        qbtn.move(50, 50)       
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Quit button')    
        self.show()



Mostramos ahora la ventana

ex=Example2()



A veces es necesario mostrar ventanas con mensajes

class Example3(QtGui.QWidget):
    def __init__(self):
        super(Example3, self).__init__()
        self.initUI()
    def initUI(self):               
        self.setGeometry(300, 300, 250, 150)        
        self.setWindowTitle('Message box')    
        self.show()
    def closeEvent(self, event):
        reply = QtGui.QMessageBox.question(self, 'Message',
            "Are you sure to quit?", QtGui.QMessageBox.Yes | 
            QtGui.QMessageBox.No, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()



Mostramos ahora la ventana anterior y vemos que sucede al intentar cerrarla

ex=Example3()



Ventana centrada en la pantalla

class Example4(QtGui.QWidget):
    def __init__(self):
        super(Example4, self).__init__()
        self.initUI()
    def initUI(self):               
        self.resize(250, 150)
        self.center()
        self.setWindowTitle('Center')    
        self.show()
    def center(self):
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())



Mostramos ahora la ventana anterior

ex=Example4()



Barra de estado

class Example5(QtGui.QMainWindow):
    def __init__(self):
        super(Example5, self).__init__()
        self.initUI()
    def initUI(self):               
        self.statusBar().showMessage('Ready')
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Statusbar')    
        self.show()



Mostramos la ventana anterior

ex=Example5()



Barra de menú

class Example6(QtGui.QMainWindow):
    def __init__(self):
        super(Example6, self).__init__()
        self.initUI()
    def initUI(self):               
        exitAction = QtGui.QAction(QtGui.QIcon('icons/ic_lock_power_off.png'), '&Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)
        self.statusBar()
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Menubar')    
        self.show()



Mostramos la ventana anterior

ex=Example6()



Toolbar

class Example7(QtGui.QMainWindow):
    def __init__(self):
        super(Example7, self).__init__()
        self.initUI()
    def initUI(self):               
        exitAction = QtGui.QAction(QtGui.QIcon('icons/ic_lock_power_off.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.triggered.connect(self.close)
        self.toolbar = self.addToolBar('Exit')
        self.toolbar.addAction(exitAction)
        self.setGeometry(300, 300, 350, 250)
        self.setWindowTitle('Toolbar')    
        self.show()
		



Mostramos

ex=Example7()



Menú y Toolbar juntos con un QTextEdit

class Example8(QtGui.QMainWindow):
    def __init__(self):
        super(Example8, self).__init__()
        self.initUI()
    def initUI(self):               
        textEdit = QtGui.QTextEdit()
        self.setCentralWidget(textEdit)
        exitAction = QtGui.QAction('Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)
        self.statusBar()
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAction)
        toolbar = self.addToolBar('Exit')
        toolbar.addAction(exitAction)
        self.setGeometry(300, 300, 350, 250)
        self.setWindowTitle('Main window')    
        self.show()



Mostramos

ex=Example8()



Layout abosoluto

class Example9(QtGui.QWidget):
    def __init__(self):
        super(Example9, self).__init__()
        self.initUI()
    def initUI(self):
        label1 = QtGui.QLabel('Zetcode', self)
        label1.move(15, 10)
        label2 = QtGui.QLabel('tutorials', self)
        label2.move(35, 40)
        label3 = QtGui.QLabel('for programmers', self)
        label3.move(55, 70)        
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Absolute')    
        self.show()



Mostramos

ex=Example9()



vbox layout

class Example10(QtGui.QWidget):
    def __init__(self):
        super(Example10, self).__init__()
        self.initUI()
    def initUI(self):
        okButton = QtGui.QPushButton("OK")
        cancelButton = QtGui.QPushButton("Cancel")
        hbox = QtGui.QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(okButton)
        hbox.addWidget(cancelButton)
        vbox = QtGui.QVBoxLayout()
        vbox.addStretch(1)
        vbox.addLayout(hbox)
        self.setLayout(vbox)    
        self.setGeometry(300, 300, 300, 150)
        self.setWindowTitle('Buttons')    
        self.show()



Mostramos

ex=Example10()



Grid layout

class Example11(QtGui.QWidget):
    def __init__(self):
        super(Example11, self).__init__()
        self.initUI()
    def initUI(self):
        names = ['Cls', 'Bck', '', 'Close', '7', '8', '9', '/',
                '4', '5', '6', '*', '1', '2', '3', '-',
                '0', '.', '=', '+']
        grid = QtGui.QGridLayout()
        j = 0
        pos = [(0, 0), (0, 1), (0, 2), (0, 3),
                (1, 0), (1, 1), (1, 2), (1, 3),
                (2, 0), (2, 1), (2, 2), (2, 3),
                (3, 0), (3, 1), (3, 2), (3, 3 ),
                (4, 0), (4, 1), (4, 2), (4, 3)]
        for i in names:
            button = QtGui.QPushButton(i)
            if j == 2:
                grid.addWidget(QtGui.QLabel(''), 0, 2)
            else: grid.addWidget(button, pos[j][0], pos[j][1])
            j = j + 1
        self.setLayout(grid)   
        self.move(300, 150)
        self.setWindowTitle('Calculator')    
        self.show()



Mostramos

ex=Example11()



Grid layout

class Example12(QtGui.QWidget):
    def __init__(self):
        super(Example12, self).__init__()
        self.initUI()
    def initUI(self):
        title = QtGui.QLabel('Title')
        author = QtGui.QLabel('Author')
        review = QtGui.QLabel('Review')
        titleEdit = QtGui.QLineEdit()
        authorEdit = QtGui.QLineEdit()
        reviewEdit = QtGui.QTextEdit()
        grid = QtGui.QGridLayout()
        grid.setSpacing(10)
        grid.addWidget(title, 1, 0)
        grid.addWidget(titleEdit, 1, 1)
        grid.addWidget(author, 2, 0)
        grid.addWidget(authorEdit, 2, 1)
        grid.addWidget(review, 3, 0)
        grid.addWidget(reviewEdit, 3, 1, 5, 1)
        self.setLayout(grid) 
        self.setGeometry(300, 300, 350, 300)
        self.setWindowTitle('Review')    
        self.show()



Mostramos

ex=Example12()
