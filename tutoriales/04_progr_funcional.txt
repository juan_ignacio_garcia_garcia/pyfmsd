Creado por ignacio.garcia@uca.es
fecha: 2015-03-09

Programación funcional




En python las funciones pueden devover funciones. 
Vamos a ver en este tutorial como manejar funciones como si fueran
variables de cualquier otro tipo. 
A este tipo de programación se le conoce como programación funcional.

pass




Definimos

def saludar(idioma):
	def s_es():
		print 'hola'
	def s_en():
		print 'hello'
	def s_fr():
		print 'salut'
	if(idioma=='en'):
		return s_en
	elif(idioma=='fr'):
		return s_fr
	else:
		return s_es




Ejecutamos:

saludar('es')()




Map, filter y reduce. 
La función map sirve para aplicar una función a cada uno de los elementos de una secuencia y 
obtener así una nueva lista.

def cuadrado(x):
	return x**2  
print map(cuadrado,range(1,5))






La función filter devuelve los elementos de una secuencia que verifican una
determinada condición.

def es_par(x):
	if(x%2==0):
		return True
	else:
		return False
print filter(es_par,range(1,10))





La función reduce aplica una función pares de elementos de una 
secuencia hasta dejarla en un su valor

def sumar(x,y):
	return x+y
l=[2,3,4,5]
print reduce(sumar,range(1,10))






Podemos definir funciones sin necesidad de darles un nombre (lambda funciones).
Son funciones sencillas con pocas llamadas que no trae cuenta definir.

print (lambda x,y: x+y)(5,7)
print map(lambda x:x**2,[1,2,3,4,5])
print filter(lambda x: x%2==0, [2,3,4,5,6,7])
print reduce(lambda x,y:x+y,[2,3,4,5])




En python también tenemos definición de listas por comprensión.
Su utilidad es la obtención de listas de forma parecida a como podemos definir conjuntos.

l=[2,3,4,5]
l2=[n**2 for n in l]
l3=[n**2 for n in l if n%2==0]
print l
print l2
print l3
a=range(1,5)
b=range(7,12)
c=[(x,y) for x in a for y in b]
d=[x+y for x in a for y in b]
e=[(x,y) for x in a if x%2==0 for y in b]
print a
print b
print c
print d
print e






Generadores. Son obtienen de forma similar a las listas por comprensión aunque en 
lugar de corchetes hay que escribir paréntesis. 
La diferencia está en que cuando definimos una lista esta se genera totalmente y los generadores no.
Suelen usarse en bucles.
Ejemplo:



a=range(10)
print a
gg=(x**2 for x in a)
for x in gg:
	print x


Otro tipo de generador es usando la función yield:

def genN(n):
 k=1
 while(n>k):
  yield k
  k=k+1


La diferencia es que al definir una lista se calculan todos sus elementos.

import time
t1=time.time()
l=range(10000000)
t2=time.time()
print t2-t1

Con el generador no calculamos nada al definir <code>1l</code>.
Conforme vayamos recorriendo <code>l1</code> se
irán calculando sus elementos

t1=time.time()
l1=genN(10000000)
t2=time.time()
print t2-t1

Una función que nos devuelve un generador es <code>xrange</code>.
Puede sustituirse en los bubles <code>for</code> para obtener
un código más eficiente.
Veamos el tiempo de ejecución de un buble <code>for</code>
con <code>range</code>.
En la orden timeit el parámetro <code>number</code>
es el número de ejecuciones de la orden dada en
en el primer argumento.

from timeit import timeit
print timeit('for x in range(1000000): pass',number=100)

Veamos el tiempo con <code>xrange</code>:

from timeit import timeit
print timeit('for x in xrange(1000000): pass',number=100)


Decoradores. Los decoradores sirven para adornar nuestras funciones. 
Son muy útiles en la depuración de nuestros programas.
Se definen así:

def miDecorador(funcion):
	print u'Llamada a miDecorador para construir una nueva función',funcion.__name__
	def nueva(*args):
		print u"Vamos a llamar a la función",funcion.__name__
		ret=funcion(*args)
		return ret
	return nueva

Se colocan justo antes de la definición de una función

@miDecorador
def faux(x,y):
	print "x+y=",x+y
	return x+y
@miDecorador
def gAux(x):
    return x**2

Veamos que sucede al llamar a <code>faux</code>.

faux(5,7)

y a <code>gAux</code>

gAux(7)

Ejercicios
<ol>
<li>
Funcion para calcular el producto escalar de dos vectores con la función map
<li>
Factorial con la función reduce
<li>
Función que transforme cadenas de caracteres de la forma
<pre>
'Fecha_dato1;valor1\nFecha_dato2;valor2\n...\nFecha_datoN;valorN'
</pre>
a listas de la forma
<pre>
[día_dato1,mes_dato1,año_dato1,parte_entera_valor1],
[día_dato2,mes_dato2,año_dato2,parte_entera_valor2],
...
[día_datoN,mes_datoN,año_datoN,parte_entera_valorN],
</pre>
<li>Quicksort con <code>filter</code> y listas por comprensión.
<li>Producto de matrices.
</ol>


pass
