Creado por ignacio.garcia@uca.es
fecha: 2015-11-10

Matplotlib (http://www.labri.fr/perso/nrougier/teaching/matplotlib/
http://matplotlib.org/api/pyplot_summary.html)

Matplotlib es la librer�a de python m�s usada para visualizar gr�ficos en 2D.
Dentro de matplotlib tenemos a pyplot que nos va a proporcionar un acceso orientado a objetos
a matplotlib.
Para pintar nuestras gr�ficas haremos uso de algunas de la funciones de la librer�a numpy

pass

Cargamos las librer�as:

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import interactive

Creamos la ventana

plt.subplot(1,1,1)

Creamos una lista de valores entre -&Pi y &Pi

X=np.linspace(-np.pi,np.pi,256,endpoint=True)

Calculamos el coseno y el seno a esos valores

C,S=np.cos(X),np.sin(X)

Pintamos las gr�ficas:

plt.plot(X,C)
plt.plot(X,S)

Las mostramos:

#interactive(True)
plt.show()

Podemos tener m�s control de lo que aparece en la ventana.
Vamos a crear una figura de tama�o concreto con la orden plt.figure

plt.figure(figsize=(20,6), dpi=60)

Create a new subplot from a grid of 1x1
Creamos una ventana (la n�mero 1) de tama�o 1x1

plt.subplot(1,1,1)

Definimos X, C, S como antes:

X = np.linspace(-np.pi, np.pi, 256,endpoint=True)
C,S = np.cos(X), np.sin(X)

Pintamos de color azul y verde, y a�adimos una leyenda:

plt.plot(X, C, color="blue", linewidth=1.0, linestyle="-",label='Coseno')
plt.plot(X, S, color="red", linewidth=2.0, linestyle="-",label='Seno')
plt.legend(loc='upper left',frameon=True)

A�adimos anotaciones a puntos

plt.scatter(0,1,c='r',marker='2',linewidth=10)
plt.annotate('nota',xy=(0,1),arrowprops=dict(arrowstyle="->"),xytext=(+10, +30), textcoords='offset points')

Establecemos los l�mites de x e y, y las marcas en los ejes:

plt.xlim(-3.0,4.0)
plt.xticks(np.linspace(-4,4,9,endpoint=True))
plt.ylim(-1.5,1.5)
plt.yticks(np.linspace(-1,1,9,endpoint=True))



grabamos la figura:

plt.savefig("grafica.png",dpi=72)

Mostramos la ventana

plt.show()

Podemos crear ventanas con varias gr�ficas en la misma columna

plt.subplot(2,1,1)
plt.plot(X,C)
plt.subplot(2,1,2)
plt.plot(X,S)
plt.show()

con varias filas

plt.subplot(3,2,1)
plt.plot(X,C)
plt.subplot(3,2,4)
plt.plot(X,S)
plt.show()

Colocar gr�ficas donde queramos

plt.axes([.1,.2,.7,.4])
plt.plot(X,C)
plt.axes([.5,.4,.4,.3])
plt.plot(X,C)
plt.show()

Gr�ficas de barras y puntos

plt.subplot(3,2,1)
plt.plot(X,C)
plt.subplot(3,2,4)
plt.plot(X,S)
plt.subplot(3,2,2)
plt.bar([5,8,10],[12,16,6],color='r',align='center')
plt.bar([6,9,11],[6,15,7],color='b',align='center')
plt.subplot(3,2,5)
plt.scatter([5,8,10],[12,16,6],color='r')
plt.scatter([6,9,11],[6,15,7],color='b')
plt.subplot(3,2,6)
import random
plt.plot(np.linspace(-5, 5, 100),[random.random() for i in xrange(100)])
plt.show()
