# -*- coding: utf-8 -*-
"""
Created on Fri Mar 06 13:49:11 2015

@author: ignacio.garcia@uca.es
"""

from distutils.core import setup
import py2exe
setup(
    name="pyFMSD",
    # primer numero de version = version del programa
    # segundo numero de version = version de tutoriales
    version='4.7',
    description='Tutorial de python para FMSD',
    author='J.I.G.G.',
    author_email='ignacio.garcia@uca.es',
    url="https://bitbucket.org/juan_ignacio_garcia_garcia/pyfmsd",
    license='GPL',
    scripts=['pyFMSD.py'],
    packages=['tutlibs','tutoriales','icons'],
    windows=['pyFMSD.py']
)