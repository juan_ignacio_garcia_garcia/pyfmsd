# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 09:56:31 2015

@author: ignacio.garcia@uca.es
"""

import sys
from PySide import QtGui, QtCore
import os
from time import sleep

from IPython.qt.console.rich_ipython_widget import RichIPythonWidget
from IPython.qt.inprocess import QtInProcessKernelManager
#from IPython.lib import guisupport

class QIPythonWidget(RichIPythonWidget):
    """ Convenience class for a live IPython console widget. We can replace the standard banner using the customBanner argument"""
    def __init__(self,customBanner=None,*args,**kwargs):
        if customBanner!=None: self.banner=customBanner
        super(QIPythonWidget, self).__init__(*args,**kwargs)
        self.kernel_manager = kernel_manager = QtInProcessKernelManager()
        kernel_manager.start_kernel()
        #kernel_manager.kernel.gui = 'qt4'
        kernel_manager.kernel.gui = 'inline'
        self.kernel_client = kernel_client = self._kernel_manager.client()
        kernel_client.start_channels()

        self.set_default_style(colors='lightbg')

    def write_to_stdin(self, line):
        self.printText(line)
        sleep(.4)
        assert isinstance(self.kernel_client, object)
        assert isinstance(self.kernel_client.input, object)
        self.kernel_client.input(line)

    """
    def stop():
        kernel_client.stop_channels()
        kernel_manager.shutdown_kernel()
        guisupport.get_app_qt4().exit()            
        self.exit_requested.connect(stop)
    """
    
    def pushVariables(self,variableDict):
        """ Given a dictionary containing name / value pairs, push those variables to the IPython console widget """
        self.kernel_manager.kernel.shell.push(variableDict)
    def clearTerminal(self):
        """ Clears the terminal """
        self._control.clear()    
    def printText(self,text):
        """ Prints some plain text to the console """
        self._append_plain_text(text)        
    def executeCommand(self,command):
        """ Execute a command in the frame of the console widget """
        self._execute(command,False)
        
        



#import time

from Tkinter import Tk



class Example(QtGui.QWidget):
    def __init__(self):
        super(Example, self).__init__()
        self.initUI()
        
    def initUI(self):      
        #super(Example, self).__init__(parent)
        
        self.layout = QtGui.QVBoxLayout(self)
        self.linea = QtGui.QLineEdit("", self)
        #self.pt = QtGui.QPlainTextEdit(self)
        self.ip = QIPythonWidget(customBanner="Welcome to the embedded ipython console\n")
        #self.ip = QIPythonWidget1()
        self.ip.pushVariables({"foo":43,"print_process_id":os.getpid()})
        self.ip.printText("The variable 'foo' and the method 'print_process_id()' are available. Use the 'whos' command for information.")                           

        
        self.layout.addWidget(self.ip)        
        self.layout.addWidget(self.linea)        

        self.ip.clearTerminal()
        
        self.setWindowTitle('QIPythonWidget')
        self.show()
        self.linea.returnPressed.connect(self.introEntrada)
    def introEntrada(self):
        self.ip.execute(self.linea.text(),False)
        self.linea.clear()

        #r=Tk()
        #r.withdraw()
        #r.clipboard_clear()
        #r.clipboard_append(texto)
        #r.destroy()
        #self.ip.clearTerminal()
        #self.ip.executeCommand('\n')
        #self.ip.prompt_to_top()

        #self.ip.paste()
        #time.sleep(1)

        #self.ip.executeCommand(texto)


        #bb=self.ip.out_prompt
        #print 'out_prompt=',bb
        #print 'output_set=',self.ip.output_sep

        #time.sleep(.5)
        #self.ip.printText(texto+'\n')
        #self.ip.append_stream(texto+'\n')
        #cad=self.ip.export_html()




def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    ex.setGeometry(400, 400, 300, 300)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()