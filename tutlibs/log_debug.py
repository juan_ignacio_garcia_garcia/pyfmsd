# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 08:18:12 2015

@author: usuario
"""

import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

"""
logging.debug('This message should appear on the console')
logging.info('So should this')
logging.warning('And this, too')
logging.debug('This message should appear on the console')
"""

def safe_str(obj):
    """ return the byte string representation of obj """
    try:
        return str(obj)
    except UnicodeEncodeError:
        # obj is unicode
        return unicode(obj).encode('unicode_escape')



def myLog(*l):
    laux=list(l)
    laux=map(lambda x:safe_str(x),laux)
    cad=' '.join(laux)
    logging.debug(cad)


#def myLog(*l):
#    pass

#myLog('hola',5,'abc')