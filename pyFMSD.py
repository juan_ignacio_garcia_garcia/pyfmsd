#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 06 13:49:11 2015

@author: ignacio.garcia@uca.es
"""
# ...
#iconos en 
# C:\Users\usuario\adt-bundle-windows-x86_64-20140321\sdk\platforms\android-19\data\res\drawable-xhdpi
from tutlibs.log_debug import myLog
from tutlibs.txt2html import *
import os
from tutlibs.tutorial_ui import *
from  tutlibs.acerca_ui import *
from  tutlibs.QIPythonWidget import QIPythonWidget
import urllib2
#import logging
#from tutlibs.log_debug import logger
from PySide.QtGui import QMessageBox
import PySide.QtCore
from  PySide.QtCore import Qt

import logging
logging.basicConfig(format='%(levelname)s -> %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def mi_decorador(funcion):
    def nueva(*args):
        logger.debug('Llamada a la funcion '+ funcion.__name__)
        retorno = funcion(*args)
        logger.debug('Fin de funcion '+ funcion.__name__)
        return retorno
    return nueva

class Dialog(QtGui.QDialog):
    """
    Ventana para manejar a Ui_Dialog.
    Ui_Dialog es la ventana creada al pulsar la opción acerca en la 
    ventana principal
    """
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)
        self.padre=parent
        self.ui =  Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.pulsado1)
    def pulsado(self):
        os.startfile('https://bitbucket.org/juan_ignacio_garcia_garcia/pyfmsd/src/master/dist/')
    def pulsado1(self):
        logger.debug( "pulsado1")
        self.close()
        
def quitaEspIniFin(cad):
    """
    Quita todos los \\n del principio y fin de cad
    """
    if cad=='\n':
        return ''
    while cad[0]=='\n':
        cad=cad[1:]
    while cad[-1]=='\n':
        cad=cad[:-1]
    return cad
    
def ponerPuntos(cad):
    """
    Añade ... tras todos los \\n de cad salvo al último que deja como \\n
    """
    cadaux=''
    for x in cad[:-1]:
        if x=='\n':
            cadaux=cadaux+'\n...'
        else:
            cadaux=cadaux+x
    return cadaux+'\n'



class MainWindow(QtGui.QMainWindow):
    cabecera="<html lang=\'es\' xml:lang=\'es\' xmlns=\"http://www.w3.org/1999/xhtml\">"
    cabecera2="\n<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>\n"
    @mi_decorador
    def __init__(self, parent=None):
        #super(MainWindow, self).__init__(parent)
        QtGui.QMainWindow.__init__(self, parent)
        self.primero=True
        self.listaOrdenes=[]
        self.ui =  Ui_MainWindow()
        self.ui.setupUi(self)
        self.compruebaVersiones()            
        self.buscaTut()
        self.myCD=None
        self.ui.comboBox.activated.connect(self.extraeTut)
        self.ui.pushButton_5.clicked.connect(self.siguientePaso)
        self.ui.pushButton_4.clicked.connect(self.pasoAtras)
        self.ui.pushButton.clicked.connect(self.repetirPaso)
        self.reiniciarTut=True        
        self.ui.comboBox.setFocus()
        pal1 = QtGui.QPalette()
        bgc1 = QtGui.QColor(15, 40, 15)
        pal1.setColor(QtGui.QPalette.Base, bgc1)
        textc1 = QtGui.QColor(230, 230, 230)
        pal1.setColor(QtGui.QPalette.Text, textc1)

        pal2 = QtGui.QPalette()
        bgc2 = QtGui.QColor(230, 230, 230)
        pal2.setColor(QtGui.QPalette.Base, bgc2)
        self.ui.webView.setPalette(pal2)

        self.toolbar1 = self.addToolBar('tb1')
        self.toolbar2 = self.addToolBar('tb2')
        self.toolbar1.addAction(self.ui.action_Salir)
        self.toolbar2.addAction(self.ui.actionActualizar_tutoriales)
        self.toolbar2.addAction(self.ui.actionDescargar_fichero_py)
        self.toolbar2.addAction(self.ui.actionReiniciar_terminal_en_cada_tutorial)
        self.toolbar2.addAction(self.ui.actionPantalla_completa)
        self.toolbar2.addAction(self.ui.actionReiniciar_tutorial)
        self.toolbar2.addAction(self.ui.actionA_cerca_de)

        self.ui.actionActualizar_tutoriales.triggered.connect(self.descargarTutoriales)
        self.ui.actionPantalla_completa.triggered.connect(self.pantallaCompleta)
        self.pantallaCompleta=False

        self.ui.actionReiniciar_tutorial.triggered.connect(self.extraeTut)

        self.IPwidget = QIPythonWidget(customBanner="Welcome to the embedded ipython console\n")
        self.IPwidget.setMinimumSize(QtCore.QSize(500, 500))
        self.IPwidget.setObjectName("MYwidget")
        self.ui.verticalLayout_4.addWidget(self.IPwidget)

    @mi_decorador
    def pantallaCompleta(self):
        #myMW.setWindowFlags(Qt.FramelessWindowHint)
        if not self.pantallaCompleta:
            self.showFullScreen()
            self.pantallaCompleta=True
        else:
            self.showNormal()
            self.pantallaCompleta=False

    def ejecutaConPaste(self,p):
        self.r.withdraw()
        self.r.clipboard_clear()
        self.r.clipboard_append(self.lcodigo[p])
        self.IPwidget.paste()

    @mi_decorador
    def escribeEnStdin(self,p):
        orden=self.lcodigo[p]
        logger.debug('escribeEnStdin'+orden)
        self.IPwidget.write_to_stdin(orden)

    @mi_decorador
    def ejecutaOrden(self,p):
        orden=self.lcodigo[p]
        logger.debug('...'+';;;');
        #if self.lcodaux[p]==0:
        if orden[-1]!=';':
            logger.debug('ejecutaOrden:'+orden)
            self.IPwidget.execute(orden)
        else:
            logger.debug('ejecutaOrden en else:'+orden)
            self.escribeEnStdin(p)

    @mi_decorador
    def compruebaVersiones(self):
        self.versionA=self.versionActual()
        self.versionD=self.versionDisponible()
        logger.debug( "version actual:"+self.versionA)
        logger.debug( "version disponible:"+self.versionD)
        aux1=self.versionA.split('.')
        aux2=self.versionD.split('.')     
        logger.debug( "versiones de tutoriales:"+str(aux1)+str(aux2))
        if self.versionA!=self.versionD and self.versionD!='x':
            reply=QMessageBox.question(self,u'Nueva versión disponible',
                               u'Versión actual '+self.versionA+\
                                  u'\nVersión disponible '+self.versionD+
                               u'\nDescargar nueva versión?',
                               QMessageBox.Yes|QMessageBox.No,
                               QMessageBox.No)
            if reply==QMessageBox.Yes:
                logger.debug('Actualizar')
                os.startfile('https://bitbucket.org/juan_ignacio_garcia_garcia/pyfmsd/src/master/dist/')
                self.close()
            else:
                logger.debug('No actualizar')

    @mi_decorador
    def closeEvent(self, event):
        self.close()
        pass
        
    @mi_decorador
    def close(self):
        logger.debug( "Llamando a close")
        try:
            self.IPwidget.kernel_client.stop_channels()
            self.IPwidget.kernel_manager.shutdown_kernel()
        except:
            pass
        QtGui.QMainWindow.close(self)
        sys.exit(0)

    def rt(self):
        logger.debug("Reiniciar terminal")
        self.reiniciarTut=not self.reiniciarTut
        logger.debug( self.reiniciarTut)
        if self.reiniciarTut:
            logger.debug( 111)
            icon4 = QtGui.QIcon()
            icon4.addPixmap(QtGui.QPixmap("icons/btn_check_on_holo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.ui.actionReiniciar_terminal_en_cada_tutorial.setIcon(icon4)
        else:
            logger.debug( 222)
            icon4 = QtGui.QIcon()
            icon4.addPixmap(QtGui.QPixmap("icons/btn_check_off_holo_light.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
            self.ui.actionReiniciar_terminal_en_cada_tutorial.setIcon(icon4)

    def graba(self):
        laux=list(self.listaOrdenes)
        laux=''.join(laux)
        logger.debug( laux)
        fich=open(self.fname,'w')
        fich.write(laux)
        fich.close()
        
    def grabarPY(self):
        logger.debug( "Grabar py")
        logger.debug( self.listaOrdenes)
        if self.fname==None:
            self.grabarComo()
        else:
            self.graba()            

    def grabarComo(self):
        logger.debug( "Grabar py como...")
        logger.debug( self.listaOrdenes)
        self.fname,aux=QtGui.QFileDialog.getSaveFileName(self,caption='Guardar como',dir=os.getcwd(),
                                                    filter='*.py')
        logger.debug( self.fname)
        logger.debug( aux)
        self.graba()

    def acercaSlot(self):
        logger.debug( "hola"+ type(self)+str(self.geometry()))
        xx=self.geometry()
        #logger.debug( str(xx.x())+str(xx.y()),xx.width(),xx.height())
        x,y,an,al=xx.x(),xx.y(),xx.width(),xx.height()
        self.myCD=Dialog()
        #logger.debug( type(self.myCD.ui.pushButton))
        self.myCD.setGeometry(x+an//2-140,y+al//2-60,280,120)
        self.myCD.setFixedSize(280,120)
        self.myCD.setWindowTitle('Acerca de...')
        self.myCD.show()
        #logger.debug( "myCD-geometry:",self.myCD.geometry())

    def extraeV(self,cad):
        lineas=cad.split('\n')
        lineas=map(lambda x:x.lstrip().rstrip(),lineas)
        lv=filter(lambda x:x[:7]=='version',lineas)
        version=lv[0][9:-2]
        return version
    def versionActual(self):
        logger.debug(sys.argv)
        caux= sys.argv[0][:-9]
        f=open(caux+"setup.py")
        cad=f.read()
        f.close()
        return self.extraeV(cad)
        
    def versionDisponible(self):
        try:
            f=urllib2.urlopen('https://bitbucket.org/juan_ignacio_garcia_garcia/pyfmsd/raw/master/setup.py')
            cad=f.read()
            f.close()
            return self.extraeV(cad)
        except Exception as e:
            logger.debug( "No se pudo conectar")
            return 'x'
            
    @mi_decorador
    def descargarTutoriales(self):
        try:
            cadaux='https://bitbucket.org/juan_ignacio_garcia_garcia/pyfmsd/raw/master/tutoriales/'
            f=urllib2.urlopen('https://bitbucket.org/juan_ignacio_garcia_garcia/pyfmsd/raw/master/MANIFEST')
            cad=f.read()
            f.close()
            lineas=cad.split('\n')
            for x in lineas:
                logger.debug('1'+ x)
            lineas=map(lambda x:x.rstrip().lstrip(),lineas)
            for x in lineas:
                logger.debug('2'+ x)
            lineas=filter(lambda x:x[:11]=='tutoriales\\' and \
                          x[-5:]=='.html', lineas)
            for x in lineas:
                logger.debug('3'+ x)
            lineas=map(lambda x:x[11:],lineas)
            for x in lineas:
                logger.debug( x)
            for nn in lineas:
                logger.debug( cadaux+nn)
                fbb=urllib2.urlopen(cadaux+nn)
                cad=fbb.read()
                fbb.close()
                f=open(".\\tutoriales\\"+nn,'w')
                f.write(cad)
                f.close()
                
        except Exception as e:
            logger.debug( "No se pudieron bajar los tutoriales")
        self.buscaTut()
        try:
            self.IPwidget.restart_kernel('Reiniciando kernel',now=True)
            self.IPwidget.reset(clear=True)
        except Exception as e:
            logger.debug(str(e))

#decorador
    @mi_decorador
    def buscaTut(self):
        self.paso=-2
        ldir=os.listdir(".")
        self.ui.comboBox.clear()
        if ldir.count("tutoriales"):
            lt=os.listdir("./tutoriales")
            lt=filter(lambda x: x[-5:]=='.html',lt)
            self.ui.comboBox.addItem('<Listado de temas>')
            for x in lt:
                self.ui.comboBox.addItem(x)
            self.statusBar().showMessage(u"Tutoriales añadidos")
            self.ui.webView.setStyleSheet("QWebView {border: 1px black solid;}")
            self.ui.webView.setHtml("""
                <html><body style='margin: 0pt; background:white; border: 1px black solid;' >
                <div style='margin: 10;'>
                    <h3 style='border: 0px black solid; '>Elija tema para comenzar<h3>
                </div>
                </body>
                """)
            self.statusBar().showMessage("Elija tema para comenzar")
        else:
            logger.debug( "Directorio tutoriales no encontrado")
            self.statusBar().showMessage("Temas no encontrados")
            self.ui.webView.setHtml("<h2>Temas no encontrados<h2>")
            
    def htmlAux(self,p):
        cad=self.cabecera+"<html><head>"+self.cabecera2+\
                "</head><body style='margin: 0;"+\
                " background:white; " +\
                "border: 1px black solid;' >"+\
                "<div style='margin: 10;'>"+\
                '<b>'+self.titulo\
                +'</b><br>'+\
                self.lsecciones[p]+'</div>'+\
                "<pre style='margin: 5; padding: 10px;"+\
                "background: #fcfcfc;" +\
                " border: 1px #aaaaaa solid;'>"\
                +self.lcodigo[p]+'</pre>'+\
                '</body></html>'
        return cad

    def htmlAux1(self,p):
        cad=self.cabecera+"<html><head>"+self.cabecera2+\
            "</head><body style='margin: 0;"+\
            " background:white; " +\
            "border: 1px black solid;"+ \
            "padding: 10px; '>"+'<b>'+self.titulo\
            +'</b><br>'

        for i in range(p+1):
            cad=cad+\
                "<div style='margin: 10px;' id='p"+str(i)+"'>"+\
                self.lsecciones[i]+'</div>'+\
                "<pre style='margin: 5; padding: 10px;"+\
                "background: #fcfcfc;" +\
                " border: 1px #aaaaaa solid;'>"\
                +self.lcodigo[i]+'</pre><br>'

        cad=cad+'</body></html>'
        return cad


    def extraeTut(self):
        pagina=''
        if self.reiniciarTut:
            self.fname=None
        self.ui.pushButton_5.setFocus()
        
        #logger.debug( "extraeTut"+self.ui.comboBox.currentText())
        nomfich=self.ui.comboBox.currentText()
        self.titulo,self.lsecciones,self.lcodigo,self.lcodaux,self.about=obtenerListas('tutoriales\\'+nomfich)
        #logger.debug( "titulo:",self.titulo.encode('ascii','replace'))
        #logger.debug( "about:",self.about.encode('ascii','replace'))
        #logger.debug( "lcodaux:",self.lcodaux)
        for x in self.lcodigo:
            logger.debug( x)
        
        self.paso=0
        self.lcodigo=map(lambda x:x.lstrip(),self.lcodigo)
        
        #logger.debug( "titulo:",self.titulo)
        #logger.debug( "seccion 0:",self.lsecciones[0])
        #logger.debug( "codigo 0:",self.lcodigo[0])
        
        if self.reiniciarTut and not self.primero:
            self.listaOrdenes=[]
            self.IPwidget.restart_kernel('Reiniciando kernel',now=True)
            self.IPwidget.reset(clear=True)
        self.primero=False
        self.ui.webView.setStyleSheet(".test{border: 1px black solid;}")

        self.ui.webView.setHtml(self.htmlAux1(0))
        self.ui.webView.page().mainFrame().scrollToAnchor('p'+str(0))

        logger.debug('webView realizado')
        #logger.debug( len(self.titulo)        )
        self.statusBar().showMessage("Tutorial: "+self.titulo.rstrip())    
        self.ejecutaOrden(0)

        logger.debug( 'CODIGO:'+self.lcodigo[0] )
        self.listaOrdenes.append(self.lcodigo[0])
    def repetirPaso(self):
        self.paso=self.paso-1
        self.siguientePaso()
    def siguientePaso(self):
        if self.paso==-2:
            logger.debug('No hay tutorial seleccionado')
            return
        self.paso=self.paso+1
        p=self.paso
        if p<len(self.lcodigo):
            #logger.debug( "seccion",p,":",self.lsecciones[p])
            #logger.debug( "codigo",p,":",self.lcodigo[p])
            self.ui.webView.setHtml(self.htmlAux1(p))
            #self.ui.webView.page().\
            #    setLinkDelegationPolicy(QtWebKit.QWebPage.dele)
            self.ui.webView.page().mainFrame().scrollToAnchor('p'+str(p))
            self.ejecutaOrden(p)
            self.listaOrdenes.append(self.lcodigo[p])
        else:
            self.statusBar().showMessage("Fin: "+self.about)
            logger.debug( "final alcanzado")
    def pasoAtras(self):
        if(self.paso==0):
            return
        self.paso=self.paso-2
        self.siguientePaso()        
        
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myMW = MainWindow()
    myMW.setWindowTitle('pyFMSD')
    myMW.setGeometry(5,30,950,700)


    myMW.show()
    sys.exit(app.exec_())
