# -*- coding: utf-8 -*-
"""
Created on Mon Mar 09 06:40:37 2015

@author: ignacio.garcia@uca.es
"""

from __future__ import print_function
from BeautifulSoup import BeautifulSoup
from tutlibs.log_debug import myLog

def leeFichTxt(nomFich):
    f=open(nomFich)
    cad=f.readlines()
    f.close()
    lcad=[]
    laux=[]
    while cad[0]=='\n' or cad[0]=='\r\n':
        cad=cad[1:]
    while cad[-1]=='\n' or cad[-1]=='\r\n':
        cad=cad[:-1]
    #print cad
    for i in range(len(cad)):
        if cad[i]!='\n':
            laux.append(cad[i])
        elif laux:
            #print aux
            lcad.append(''.join(laux))
            laux=[]
    if laux:
        lcad.append(''.join(laux))
        
    return lcad
    
def creaHtml(lcad):
    cad="<html lang=\'es\' xml:lang=\'es\' xmlns=\"http://www.w3.org/1999/xhtml\">\n"
    cad=cad+'<meta name="description" content="' +lcad[0]+'">'
    cad=cad+'<html>\n<head><title>\n'+lcad[1]+'</title></head>\n<body>\n'
    #print range(2,len(lcad),2)
    for i in range(2,len(lcad),2):
        cad=cad+'\n<p>\n'+lcad[i][:-1]+'\n<pre>\n'+lcad[i+1]+'</pre>\n</p>\n'
    cad=cad+'\n</body>\n</html>'
    return cad
    
#import codecs
    
def obtenerListas(nomFich):
    """
    nomFich='tutoriales\\mysql.html'
    titulo,lsecciones,lcodigo,lcodaux,about=obtenerListas(nomFich)
    """
    f=open(nomFich)
    cad=f.read()
    f.close()
    soup = BeautifulSoup(cad)

    titulo=unicode(soup.title.contents[0])
    lps=soup.findAll('html')[1].findAll('p')
    #lsecciones=map(lambda x:unicode(x),lps)
    
        
    laux=map(lambda x:x.contents,soup.findAll('p'))
    for i in range(len(laux)):
        laux[i]=filter(lambda x:x!='\n',laux[i])
        #lsecciones[i]=map(x.contents,lsecciones[i])

    myLog("len(laux)=",len(laux))
    for i in range(len(laux)):
        myLog("laux",i,">>>",str(laux[i][-1])[6:-7])
    myLog("laux",map(len,laux))

    lcodigo=map(lambda x: x[-1].string.rstrip(),laux)
    #lcodigo=map(lambda x: str(x[-1])[6:-6],laux)
    lcodaux=map(lambda x:len(x.attrs),soup.findAll('pre'))

    myLog(lcodigo)
    myLog(lcodaux)

    laux=map(lambda x:map(unicode,x),laux)
    lsecciones=map(lambda x:'\n'.join(x[:-1]),laux)

    about=unicode(soup.meta['content'])

    return titulo,lsecciones,lcodigo,lcodaux,about
    
def safe_str(obj):
    """ return the byte string representation of obj """
    try:
        return str(obj)
    except UnicodeEncodeError:
        # obj is unicode
        return unicode(obj).encode('unicode_escape')

    
import sys
def main():
    nomfich=sys.argv[1]
    if(nomfich[-4:]!='.txt'):
        print(u'El fichero no es .txt')
        return 
    print(u"Creando: "+nomfich[:-3]+"html")
    lcad=leeFichTxt(nomfich)
    print(u"Longitud lcad:",len(lcad))
    if len(lcad)%2==0:
        cad=creaHtml(lcad)
        f=open(nomfich[:-3]+'html',"w")
        f.write(cad)
        f.close()
        print(u"Título: ",safe_str(lcad[1]).rstrip())
        print(u"Descripción",safe_str(lcad[0]))
    else:
        print(u"Error en fichero .txt")
        print( u'El numero separaciones no es par')


"""
f=open('..\\tutoriales\\03_funciones.html')
cad=f.read()
f.close()
"""

"""
soup = BeautifulSoup(cad)
titulo=soup.title.string
lps=soup.findAll('html')[1].findAll('p')
lsecciones=map(lambda x:x.contents,soup.findAll('p'))
for i in range(len(lsecciones)):
    lsecciones[i]=filter(lambda x:x!='\n',lsecciones[i])
    
primerP=soup.html.nextSibling.p
siguientes=primerP.findNextSiblings()
segundoP=primerP.nextSibling
tercerP=segundoP.nextSibling
"""


"""    
cad= creaHtml(leeFichTxt('txt1.txt'))
from BeautifulSoup import BeautifulSoup
soup = BeautifulSoup(cad)
titulo=soup.title.contents[0]
lsecciones=map(str,soup.findAll('p'))
lcodigo=soup.findAll('pre')
lcodigo=map(lambda x:x.contents[0],lcodigo)
about=soup.meta['content']
"""

if __name__=="__main__":
    main()