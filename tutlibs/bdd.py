#import mysql.connector
from mysql.connector import (connection)

class MyBDWidget():
    def __init__(self):
        self.cnx=self.conectar()
        self.q=''
        self.cursor=self.cnx.cursor()
        self.resultado=None
        self.salidaPantalla=''
    @property
    def getConector(self):
        return self.cnx
    def conectar(self):
        cnx = connection.MySQLConnection(user='root', password='holaroot', host='127.0.0.1')
        return cnx

    def desconectar(self):
        self.cnx.close()
    @property
    def getCursor(self):
        return self.cursor

    def ejecutarSinMostrar(self,q):
        self.q=q
        if self.q:
            self.cursor.execute(self.q)
    def ejecutarYmostrar(self,q):
        try:
            self.ejecutarSinMostrar(q)
            self.salidaPantalla=''
            self.salidaPantalla=self.salidaPantalla+'\n ___'+''.join(['_' for x in self.q])+'_____\n'
            self.salidaPantalla=self.salidaPantalla+' >>> '+self.q+' >>>\n'
            self.salidaPantalla=self.salidaPantalla+' ---'+''.join(['-' for x in self.q])+'-----\n'
            self.resultado=None
        except Exception as e:
            print "Error en la orden:\n", e
            self.salidaPantalla=str(e)
            return
        try:
            self.resultado=self.cursor.fetchall()
            for x in self.resultado:
                v=10
                for y in x[:-1]:
                    self.salidaPantalla=self.salidaPantalla+'%17s'%y
                self.salidaPantalla=self.salidaPantalla+ '%17s\n'%x[-1]
        except Exception as e:
            #print "Orden sin datos de salida:\n", e
            self.salidaPantalla=str(e)
            return
        print self.salidaPantalla

def seguir(entrada):
    if len(entrada)==0:
        return True
    if entrada[-1]!=';':
        return True
    return False

def main():
    aux=MyBDWidget()
    entrada=raw_input('mysql> ')
    while(entrada!='quit;'):
        while seguir(entrada):
            entrada=entrada+''+raw_input('...... ')
        aux.ejecutarYmostrar(entrada)
        entrada=raw_input('mysql> ')
    #aux.ejecutarYmostrar('use bd2')
    #aux.ejecutarYmostrar('show tables;')
    #aux.ejecutarYmostrar('select * from alumnos;')
    #aux.ejecutarYmostrar('select * from alumnos11;')


if __name__=='__main__':
    main()

